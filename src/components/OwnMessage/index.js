import React from 'react';
import './OwnMessage.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrashAlt  } from "@fortawesome/free-solid-svg-icons";
import { getTimeFromISO } from '../../helpers/datesHelper';

class OwnMessage extends React.Component {

  id = this.props.message.id;

  onDelete() {
    this.props.deleteMessage(this.id);
  }

  onEdit() {
    this.props.setCurrentId(this.id);
    this.props.showEditModal();
  }

  render() {
    const { createdAt } = this.props.message;
    const text = this.props.message.text;
    return (
      <div className="own-message">
        <div className="message-body">
          <div className="message-text">{ text }</div>
        </div>
        <div className="message-metadata">
          <span className="message-time">{ getTimeFromISO(createdAt) }</span>
          <div className="message-edit" onClick={ this.onEdit.bind(this) }><FontAwesomeIcon icon={ faPen } /></div>
          <div className="message-delete" onClick={ this.onDelete.bind(this) }><FontAwesomeIcon icon={ faTrashAlt } /></div>
        </div>
      </div>
    );
  }
}
  
export default OwnMessage;
