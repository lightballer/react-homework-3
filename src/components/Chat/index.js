import React from 'react';
import { connect } from 'react-redux';
import './Chat.css';
import Header from '../Header';
import MessageList from '../MessageList';
import MessageInput from '../MessageInput';
import Preloader from '../Preloader';
import EditModal from '../EditModal';
import { addMessage } from './actions';
import { showPreloader, hidePreloader } from '../Preloader/actions';
import { showEditModal, hideEditModal } from '../EditModal/actions';

class Chat extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
    };
  }
  
  componentDidMount() {
    fetch(this.props.url)
      .then(response => response.json())
      .then(
        data => {
          data.map(item => this.props.addMessage(item.id, item));
          this.props.hidePreloader();
        },
        error => {
          this.setState({
             error,
          });
          this.props.hidePreloader();
      });
  }

  render() {
    const { error } = this.state;
    const messages = this.props.messages;
    if (error) {
      return <p>Error to fetch!</p>
    } else if (this.props.preloader) {
      return <Preloader/>;
    } else {
      const participantsCount = messages.reduce((acc, cur) => {
        if (!acc.includes(cur.userId))
          acc.push(cur.userId);
          return acc;
        }, []).length;
      let lastMessageTime = null;
      if (messages.length) {
        const lastMessage = messages[messages.length - 1];
        lastMessageTime = lastMessage.createdAt;
      }
      return (
        <div className="App">
          <EditModal/>
          <Header 
            participantsCount={ participantsCount } 
            messagesCount={ messages.length } 
            lastMessageTime={ lastMessageTime } />
          <MessageList />
          <MessageInput />
        </div>
      ); 
    }
  }
}

const mapStateToProps = state => ({
  messages: state.chat.messages,
  preloader: state.chat.preloader,
  editModal: state.chat.editModal,
});

const mapDispathToProps = {
  addMessage,
  showPreloader,
  hidePreloader,
  showEditModal,
  hideEditModal,
}

export default connect(mapStateToProps, mapDispathToProps)(Chat);
