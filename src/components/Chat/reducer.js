import { ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE } from './actionTypes';
import { SHOW_PRELOADER, HIDE_PRELOADER } from '../Preloader/actionTypes';
import { SHOW_EDIT_MODAL, HIDE_EDIT_MODAL, SET_CURRENT_ID, DROP_CURRENT_ID } from '../EditModal/actionTypes';

const initialState = {
  messages: [],
  editModal: false,
  preloader: true,
  currentId: '',
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_MESSAGE: {
      const { id, data } = action.payload;
      if (state.messages.some(item => item.id === id)) return state;
      const newMessage = { id, ...data };
      const newMessages = [...state.messages, newMessage];
      return { 
        ...state, 
        messages: newMessages
      };
    }
    case EDIT_MESSAGE: {
      const { id, data } = action.payload;
      const updatedMessages = state.messages.map(message => {
        if (message.id === id) {
          return { ...message, ...data };
        } else {
          return message;
        }
      });
      return { 
        ...state, 
        messages: updatedMessages 
      };
    }
    case DELETE_MESSAGE: {
      const { id } = action.payload;
      const filteredMessages = state.messages.filter(message => message.id !== id);
      return { 
        ...state, 
        messages: filteredMessages 
      };
    }
    case SHOW_PRELOADER: {
      return { 
        ...state, 
        preloader: true
      };
    }
    case HIDE_PRELOADER: {
      return { 
        ...state, 
        preloader: false
      };
    }
    case SHOW_EDIT_MODAL: {
      return { 
        ...state, 
        editModal: true 
      };
    }
    case HIDE_EDIT_MODAL: {
      return { 
        ...state, 
        editModal: false 
      };
    }
    case SET_CURRENT_ID: {
      const { id } = action.payload;
      return { 
        ...state,
        currentId: id,
      }
    }
    case DROP_CURRENT_ID: {
      return { 
        ...state,
        currentId: '',
      }
    }
    default:
      return state;
  }
}