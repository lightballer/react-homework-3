import { ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE } from './actionTypes';

export const addMessage = (id, data) => ({
  type: ADD_MESSAGE,
  payload: {
    id,
    data,
  }
});

export const editMessage = (id, data) => ({
  type: EDIT_MESSAGE, 
  payload: {
    id,
    data,
  }
});

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  payload: {
    id,
  }
});
