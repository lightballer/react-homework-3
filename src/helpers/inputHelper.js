export function setInputValue(event, keyword) {
  const value = event.target.value;
  this.setState({
    [keyword]: value,
  });
}